import json
import requests

import aiohttp
from aiohttp import web, ClientSession
import aiohttp_cors
import asyncio
import nest_asyncio
# import requests_async as requests

# nest_asyncio.apply()

async def create_client_session(app):
    app['client_session'] = ClientSession(read_timeout=20 * 60)

async def tr(data,url):
    
    session = app['client_session']
    async with session.post(url,json=data) as resp:
        resp: web.json_response
        r = await resp.json()
        print(resp.status)
        status = resp.status
        print(r)
    return {"status": status, "resp": r}

routes = web.RouteTableDef()

@routes.post('/her')
async def her(request):
    data = await request.json()
    return web.json_response({"sis'ka": "pipis'ka"})

@routes.post('/trainstatus')
async def her(request):
    data = await request.json()

    redis_trainstatus = 'http://40.114.54.96:8003/get_trainstatus'
    if "storyid" not in data:
        web.json_response({"error": "not story"})
    if "unitid" not in data:
        response = requests.post(redis_trainstatus, data=json.dumps({"story":data['storyid']}))
    else:
        response = requests.post(redis_trainstatus, data=json.dumps({"story": data['storyid'], "unit": data['unitid']}))

    return web.json_response(response.json())

@routes.post('/train')
async def train_story(request):
    data = await request.json()
    print(data)
    url = "http://52.170.194.186:6000/train"
    models_story = "models/"+str(data['storyid'])
    
    # loop = asyncio.get_event_loop()

    task_train = asyncio.ensure_future(tr(data, url))
    # await task_train
    try:
        await task_train
        resp_train = task_train.result()
        # result = future.result(timeout)
    except:
        print('The coroutine took too long, cancelling the task...')
        # task_train.cancel()
        with open(models_story+'/model_acc_loss.json') as json_file:
            r = json.load(json_file)
        resp_train = {"status":200, "resp": r}
    

    if resp_train["status"] != 200:
        return web.json_response({"error": "train NLU error"})
    else:
        return web.json_response(resp_train["resp"])

@routes.post('/smalltalkurl')
async def smalltalk(request):
    data = await request.json()
    audiourl = data['url']

    stturl = "http://13.92.90.125:8002/stturl"
    smtkurl = "http://52.170.194.186:6001/smalltalk"

    
    response = requests.post(stturl, data=json.dumps({"url":audiourl}))
    if response.status_code == 200:
        text = response.json()['data']
    else:
        return web.json_response({"error": "ASR error"})

    smtk = asyncio.ensure_future(tr({"text":text}, smtkurl))
    # await task_train
    await smtk
    response = smtk.result()
    print(response)
    
     #{"outputs": outputs, "big_idx": big_idx, "big_val": big_val, "text": text, "correct_text": cortext}
    if response["status"] != 200:
        return web.json_response({"error": "recognition in NLU error"})
    else:
        return web.json_response(response["resp"])

@routes.post('/smalltalk')
async def smalltalk(request):

    stturl = "http://13.92.90.125:8002/stt"
    smtkurl = "http://52.170.194.186:6001/smalltalk"

    reader = await request.multipart()
    print(reader  )

    field = await reader.next()
    #assert field.name == 'name'
    print(field)
    text = None
    nex = True
    try:
        data = await field.json() 
    except:
        nex = False

    if nex:
        print(data)
        
        if 'id' in data:
            ids = data['id']
        else:
            return web.json_response({"error": "no id"})

        field = await reader.next()

        data_f = await field.read(decode=True)
    else:
        field = await reader.next()
        filename = field.filename
        data_f = await field.read(decode=True)

    response = requests.post(stturl, files={'file':data_f}, data = {'data':json.dumps({"text":None})})
    if response.status_code == 200:
        text = response.json()['data']
    else:
        return web.json_response({"error": "ASR error"})

    smtk = asyncio.ensure_future(tr({"text":text}, smtkurl))
    # await task_train
    await smtk
    response = smtk.result()
    print(response)
    
     #{"outputs": outputs, "big_idx": big_idx, "big_val": big_val, "text": text, "correct_text": cortext}
    if response["status"] != 200:
        return web.json_response({"error": "recognition in NLU error"})
    else:
        return web.json_response(response["resp"])


@routes.post('/predict')
async def train_story(request):

    stturl = "http://13.92.90.125:8002/stt"
    classurl = "http://52.170.194.186:6001/predict"

    reader = await request.multipart()
    print(reader  )

    field = await reader.next()
    #assert field.name == 'name'
    print(field)
    text = None
    nex = True
    try:
        data = await field.json() 
    except:
        nex = False

    if nex:
        print(data)
        
        if 'storyid' in data:
            storyid = data['storyid']
        else:
            return web.json_response({"error": "no storyid"})

        if 'unitid' in data:
            unitid = data['unitid']
        else:
            return web.json_response({"error": "no unitid"})
        
        field = await reader.next()

        data_f = await field.read(decode=True)
    else:
        field = await reader.next()
        filename = field.filename
        data_f = await field.read(decode=True)

    response = requests.post(stturl, files={'file':data_f}, data = {'data':json.dumps({"text":None})})
    if response.status_code == 200:
        text = response.json()['data']
    else:
        return web.json_response({"error": "ASR error"})

    classdata = {"text":text, "storyid": storyid, "unitid": unitid}
    print(classdata)
    task_predict = asyncio.ensure_future(tr(classdata, classurl))
    # await task_train
    await task_predict
    response = task_predict.result()
    print(response)
    
     #{"outputs": outputs, "big_idx": big_idx, "big_val": big_val, "text": text, "correct_text": cortext}
    if response["status"] != 200:
        return web.json_response({"error": "recognition in NLU error"})
    else:
        return web.json_response(response["resp"])

    response = requests.post(classurl, data = {'data':json.dumps({"text":None})})    


if __name__ == "__main__":
    app = web.Application()

    app.add_routes(routes)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
    })

    for route in list(app.router.routes()):
        cors.add(route)
    #web.run_app(app)
    
    app.on_startup.append(create_client_session)

    web.run_app(app,host='0.0.0.0', port = '6003')
    
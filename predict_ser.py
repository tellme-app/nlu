from nltk.tokenize import word_tokenize

from punctuator2 import models
from punctuator2 import data

import theano
import sys
import os
import re
import json
from io import open

from aiohttp import web
import aiohttp_cors
from aiohttp import web, ClientSession
import asyncio

routes = web.RouteTableDef()

import theano.tensor as T
import numpy as np

import pandas as pd
import torch
import transformers
from torch.utils.data import Dataset, DataLoader
from transformers import ElectraModel, ElectraTokenizer

from neuspell.neuspell.corrector_elmosclstm import CorrectorElmoSCLstm
from neuspell.neuspell.corrector_bertsclstm import CorrectorBertSCLstm

from transformers import BlenderbotSmallTokenizer, BlenderbotForConditionalGeneration
mname = 'facebook/blenderbot-90M'
blender = BlenderbotForConditionalGeneration.from_pretrained(mname)
blendertok = BlenderbotSmallTokenizer.from_pretrained(mname)

from torch import cuda
device = 'cuda' if cuda.is_available() else 'cpu'

electratoken = ElectraTokenizer.from_pretrained('google/electra-small-generator')
# l1 = ElectraModel.from_pretrained("google/electra-base-discriminator")

MAX_LEN = 512
TRAIN_BATCH_SIZE = 2
VALID_BATCH_SIZE = 1
EPOCHS = 300
LEARNING_RATE = 1e-05
TOKENIZE = True
CURR_MODEL_KEYWORD = "scrnn-elmo"
CURR_MODEL = None
TOPK = 1

PRELOADED_MODELS = { "scrnn-elmo": CorrectorElmoSCLstm(tokenize=TOKENIZE, pretrained=True),
"scrnn-bert": CorrectorBertSCLstm(tokenize=TOKENIZE, pretrained=True)}


CLASSIFFIRE_MODELS = {}
ALL_CONFIG = {}

numbers = re.compile(r'\d')
is_number = lambda x: len(numbers.sub('', x)) / len(x) < 0.6

show_unk = False

x = T.imatrix('x')

print("Loading model parameters...")
net, _ = models.load("Demo-Europarl-EN.pcl", 1, x)

print("Building model...")
predict = theano.function(inputs=[x], outputs=net.y)
word_vocabulary = net.x_vocabulary
punctuation_vocabulary = net.y_vocabulary
reverse_word_vocabulary = {v:k for k,v in net.x_vocabulary.items()}
reverse_punctuation_vocabulary = {v:k for k,v in net.y_vocabulary.items()}

human_readable_punctuation_vocabulary = [p[0] for p in punctuation_vocabulary if p != data.SPACE]
tokenizer = word_tokenize
untokenizer = lambda text: text.replace(" '", "'").replace(" n't", "n't").replace("can not", "cannot")

async def create_client_session(app):
    app['client_session'] = ClientSession(read_timeout=20 * 60)

def predict_spell(message, model):
#     global CURR_MODEL, CURR_MODEL_KEYWORD, TOPK
    if TOPK==1:
        message_modified, result = PRELOADED_MODELS[model].correct_string(message, return_all=True)
        return result

def to_array(arr, dtype=np.int32):
    # minibatch of 1 sequence as column
    return np.array([arr], dtype=dtype).T

def convert_punctuation_to_readable(punct_token):
    if punct_token == data.SPACE:
        return ' '
    elif punct_token.startswith('-'):
        return ' ' + punct_token[0] + ' '
    else:
        return punct_token[0] + ' '

def punctuate(predict, word_vocabulary, punctuation_vocabulary, reverse_punctuation_vocabulary, reverse_word_vocabulary, words, show_unk):

    if len(words) == 0:
        sys.exit("Input text from stdin missing.")

    if words[-1] != data.END:
        words += [data.END]

    i = 0
    sentence = ""

    while True:

        subsequence = words[i:i+data.MAX_SEQUENCE_LEN]

        if len(subsequence) == 0:
            break

        converted_subsequence = [word_vocabulary.get(
                "<NUM>" if is_number(w) else w.lower(),
                word_vocabulary[data.UNK])
            for w in subsequence]

        if show_unk:
            subsequence = [reverse_word_vocabulary[w] for w in converted_subsequence]

        y = predict(to_array(converted_subsequence))

        sentence += subsequence[0].title()

        last_eos_idx = 0
        punctuations = []
        for y_t in y:

            p_i = np.argmax(y_t.flatten())
            punctuation = reverse_punctuation_vocabulary[p_i]

            punctuations.append(punctuation)

            if punctuation in data.EOS_TOKENS:
                last_eos_idx = len(punctuations) # we intentionally want the index of next element

        if subsequence[-1] == data.END:
            step = len(subsequence) - 1
        elif last_eos_idx != 0:
            step = last_eos_idx
        else:
            step = len(subsequence) - 1

        for j in range(step):
            current_punctuation = punctuations[j]
            sentence += convert_punctuation_to_readable(current_punctuation)
            if j < step - 1:
                if current_punctuation in data.EOS_TOKENS:
                    sentence += subsequence[1+j].title()
                else:
                    sentence += subsequence[1+j]

        if subsequence[-1] == data.END:
            break

        i += step
    
    return sentence

def repunct(text):

    words = [w for w in untokenizer(' '.join(tokenizer(text))).split()
             if w not in punctuation_vocabulary and w not in human_readable_punctuation_vocabulary]

    return punctuate(predict, word_vocabulary, punctuation_vocabulary, reverse_punctuation_vocabulary, reverse_word_vocabulary, words, show_unk)

class ELECTRAClass(torch.nn.Module):
    def __init__(self,l1,classifier,pre_classifier):
        super(ELECTRAClass, self).__init__()
        self.l1 = l1
        self.pre_classifier = pre_classifier
        self.dropout = torch.nn.Dropout(0.3)
        self.classifier = classifier

    def forward(self, input_ids, attention_mask):
        output_1 = self.l1(input_ids=input_ids, attention_mask=attention_mask)
        hidden_state = output_1[0]
        pooler = hidden_state[:, 0]
        pooler = self.pre_classifier(pooler)
        pooler = torch.nn.ReLU()(pooler)
        pooler = self.dropout(pooler)
        output = self.classifier(pooler)
        return output

class TriagePred(Dataset):
    def __init__(self, text, tokenizer, max_len):
        self.len = 1
        self.data = text
        self.tokenizer = tokenizer
        self.max_len = max_len
        
    def __getitem__(self, index):
        title = str(self.data)
        title = " ".join(title.split())
        inputs = self.tokenizer.encode_plus(
            title,
            None,
            add_special_tokens=True,
            max_length=self.max_len,
            pad_to_max_length=True,
            return_token_type_ids=True,
            truncation=True
        )
        ids = inputs['input_ids']
        mask = inputs['attention_mask']

        return {
            'ids': torch.tensor(ids, dtype=torch.long),
            'mask': torch.tensor(mask, dtype=torch.long),
            'targets': torch.tensor(0, dtype=torch.long)
        } 
    
    def __len__(self):
        return self.len

def model_load(storyid, unitid=None, check=False):
    global CLASSIFFIRE_MODELS
    global ALL_CONFIG

    models_story = 'models/'+str(storyid)
    if storyid not in CLASSIFFIRE_MODELS:
        CLASSIFFIRE_MODELS[storyid] = {}
    if unitid is None:
        unitids = []
        tmps = os.listdir(models_story)
        for file in tmps:
            if '.json' not in file:
                unitids.append(file)
        model_file = ['models/'+str(storyid)+"/"+str(unitid) for unitid in unitids]
    else:
        model_file = ['models/'+str(storyid)+"/"+str(unitid)]
        unitids = [unitid]


    model_path = 'models/'+str(storyid)+"."+str(unitid)
    nottrain = {'unit':[]}
    if os.path.isdir(models_story):
        with open(models_story+'/data.json') as json_file:
            uuid = json.load(json_file)['uuid']
        if check:
            return uuid


        for unitid, model_path in zip(unitids, model_file):

            if os.path.isdir(model_path):
                CLASSIFFIRE_MODELS[storyid][unitid] = torch.load(model_path+'/model').to(device)
                # CLASSIFFIRE_MODELS[storyid][unitid] = ELECTRAClass(l1,
                #     torch.load(model_path+'/classifier'),
                #     torch.load(model_path+'/pre_classifier')).to(device)
            else:
                nottrain['unit'].append(unitid)


        with open(models_story+'/config.json') as json_file:
            config = json.load(json_file)

        ALL_CONFIG[storyid] = config

        return nottrain
    else:
        return "story didn't train"

async def tr(data,url):
    
    session = app['client_session']
    async with session.post(url,json=data) as resp:
        resp: web.json_response
        r = await resp.json()
        print(resp.status)
        status = resp.status
        print(r)
    return {"status": status, "resp": r}

@routes.post('/load')
async def load(request):
    data = await request.json()

    if 'unitid' in data:
        model_load(data["storyid"], data["unitid"])
    else:
        model_load(data["storyid"])

@routes.post('/corrtext')
async def corrtext(request):
    data = await request.json()

    text = data['text']

    cortext = predict_spell(repunct(predict_spell(text,'scrnn-bert')), 'scrnn-elmo')
    crt = ""
    pun = ",.!?"
    for i in reversed(cortext):
        if i == ' ' and crt[0] in pun:
            pass
        else:
            crt = i+crt

    return web.json_response({"text": crt})

    
@routes.post('/smalltalk')
async def class_predict(request):
    data = await request.json()

    text = data['text']

    UTTERANCE = predict_spell(repunct(predict_spell(text,'scrnn-bert')), 'scrnn-elmo')

    inputs = blendertok([UTTERANCE], return_tensors='pt')
    inputs.pop('token_type_ids')
    reply_ids = blender.generate(**inputs)
    ans = [blendertok.decode(g, skip_special_tokens=True, clean_up_tokenization_spaces=False) for g in reply_ids]
    
    def fix_space(ans):
        crt = ""
        pun = ",.!?'"
        pun1 = "'"
        for i in reversed(ans):
            if i == ' ' and crt[0] in pun:
                pass
            elif i == pun1 and crt[0] in ' ':
                crt = i+crt[1:]
            else:
                crt = i+crt
        return crt
    crt = fix_space(ans[0])
    crt = predict_spell(repunct(predict_spell(crt,'scrnn-bert')), 'scrnn-elmo')
    crt = fix_space(crt)
    UTTERANCE = fix_space(UTTERANCE)


    return web.json_response({"answer": crt, 
        "usersay": UTTERANCE})



@routes.post('/predict')
async def class_predict(request):
    global CLASSIFFIRE_MODELS
    global ALL_CONFIG

    cortexturl = "http://0.0.0.0:6001/corrtext"

    data = await request.json()
    storyid = str(data['storyid'])
    unitid = str(data['unitid'])

    if storyid in CLASSIFFIRE_MODELS:
        if unitid in CLASSIFFIRE_MODELS[storyid]:
            pass
        else:
            stat = model_load(storyid, unitid)
            if stat == "story didn't train":
                return web.json_response({"error": stat})
            
            if stat['unit'] != []:
                return web.json_response(stat)


    else:
        stat = model_load(storyid)
        if stat == "story didn't train":
            return web.json_response({"error": stat})
        
        if stat['unit'] != []:
            return web.json_response(stat)


    text = data['text']

    corr_train = asyncio.ensure_future(tr({"text":text}, cortexturl))
    # await task_train
    await corr_train
    response = corr_train.result()
    crt = response["resp"]["text"]

    # print(CLASSIFFIRE_MODELS)
    datapred = DataLoader(TriagePred(crt, electratoken, MAX_LEN))
    with torch.no_grad():
        for _,dat in enumerate(datapred, 0):
            outputs = CLASSIFFIRE_MODELS[storyid][unitid](dat['ids'].to(device, dtype = torch.long), 
                             dat['mask'].to(device, dtype = torch.long))
            big_val, big_idx = torch.max(outputs.data, dim=1)
            print(outputs)
            print(big_idx)
            print(big_val)


    outputs = outputs.tolist()[0][:-1]

    #"big_idx": big_idx.tolist(), "big_val": big_val.tolist(),

    return web.json_response({"outputs": outputs, "config": ALL_CONFIG[storyid][unitid],  "text": text, "correct_text": crt})


if __name__ == "__main__":
    app = web.Application()

    app.add_routes(routes)

    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
                allow_credentials=True,
                expose_headers="*",
                allow_headers="*",
            )
    })

    for route in list(app.router.routes()):
        cors.add(route)
    #web.run_app(app)
    
    app.on_startup.append(create_client_session)

    web.run_app(app,host='0.0.0.0', port = '6001')

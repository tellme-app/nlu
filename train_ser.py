import re
import os
import json
import shutil

import pandas as pd
import torch
import transformers
from torch.utils.data import Dataset, DataLoader
from transformers import ElectraModel, ElectraTokenizer
from torch import cuda

import requests
from aiohttp import web
import aiohttp_cors

routes = web.RouteTableDef()

device = 'cuda' if cuda.is_available() else 'cpu'
device

# Defining some key variables that will be used later on in the training
MAX_LEN = 512
TRAIN_BATCH_SIZE = 2
VALID_BATCH_SIZE = 1
EPOCHS = 300
LEARNING_RATE = 1e-05
tokenizer = ElectraTokenizer.from_pretrained('google/electra-small-generator')
# l1 = ElectraModel.from_pretrained("google/electra-small-generator")

class Triage(Dataset):
    def __init__(self, dataframe, tokenizer, max_len):
        self.len = len(dataframe)
        self.data = dataframe
        self.tokenizer = tokenizer
        self.max_len = max_len
        
    def __getitem__(self, index):
        title = str(self.data.TITLE[index])
        title = " ".join(title.split())
        inputs = self.tokenizer.encode_plus(
            title,
            None,
            add_special_tokens=True,
            max_length=self.max_len,
            pad_to_max_length=True,
            return_token_type_ids=True,
            truncation=True
        )
        ids = inputs['input_ids']
        mask = inputs['attention_mask']

        return {
            'ids': torch.tensor(ids, dtype=torch.long),
            'mask': torch.tensor(mask, dtype=torch.long),
            'targets': torch.tensor(self.data.ENCODE_CAT[index], dtype=torch.long)
        } 
    
    def __len__(self):
        return self.len


class ELECTRAClass(torch.nn.Module):
    def __init__(self,ls):
        super(ELECTRAClass, self).__init__()
        self.l1 = ElectraModel.from_pretrained("google/electra-small-generator")
        self.pre_classifier = torch.nn.Linear(256, 256)
        self.dropout = torch.nn.Dropout(0.3)
        self.classifier = torch.nn.Linear(256, ls)

    def forward(self, input_ids, attention_mask):
        output_1 = self.l1(input_ids=input_ids, attention_mask=attention_mask)
        hidden_state = output_1[0]
        pooler = hidden_state[:, 0]
        pooler = self.pre_classifier(pooler)
        pooler = torch.nn.ReLU()(pooler)
        pooler = self.dropout(pooler)
        output = self.classifier(pooler)
        return output


def calcuate_accu(big_idx, targets):
    n_correct = (big_idx==targets).sum().item()
    return n_correct


@routes.post('/train')
async def train_story(request):
    data = await request.json()

    train_dataset = {}
    unitid_labels = {}
    storyid = data['storyid']
    # trainid =
    for context in data['contexts']:
    #     tmp_ds = {}
        if context["unitId"] not in train_dataset:
            train_dataset[context["unitId"]] = []
            unitid_labels[context["unitId"]] = -1
        if context["unitId"] in train_dataset:
            unitid_labels[context["unitId"]] +=1
            if "to" in context:
                tmp_ds = {"label": unitid_labels[context["unitId"]],
                      "id": context["id"],
                      "to": context["to"]}
            else:
                tmp_ds = {"label": unitid_labels[context["unitId"]],
                      "id": context["id"],
                      "to": None}
                
            val = []
            for value in context['values']:
                val.append(re.sub("^\s+|\n|\r|\s+$", '', value['value']))
            tmp_ds['values'] = val
            train_dataset[context["unitId"]].append(tmp_ds)

    config = {}
    all_df = {}
    for obj in train_dataset:
        train_df = []
        config[obj] = {tds['label']: [tds['id'], tds['to']] for tds in train_dataset[obj]}
        for tds in train_dataset[obj]:
            for val in tds['values']:
                train_df.append([val,tds['label']])
        train_df = pd.DataFrame(train_df)
        train_df.columns = ["TITLE", "ENCODE_CAT"]
        all_df[obj] = train_df


    model_acc_loss = []
    output_models_story = 'models/'+str(storyid)

    try:
        os.mkdir(output_models_story)
    except:
        pass

    for obj in all_df:
        # ls = len(all_df[obj]['ENCODE_CAT'].drop_duplicates()) +1
        ls = len(config[obj]) + 1
        traindf = all_df[obj]
        training_set = Triage(train_df, tokenizer, MAX_LEN)

        train_params = {'batch_size': TRAIN_BATCH_SIZE,
                        'shuffle': True,
                        'num_workers': 0
                        }

        training_loader = DataLoader(training_set, **train_params)

        model = ELECTRAClass(ls)
        model.to(device)

        loss_function = torch.nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(params =  model.parameters(), lr=LEARNING_RATE)


        def train(epoch):
            tr_loss = 0
            n_correct = 0
            nb_tr_steps = 0
            nb_tr_examples = 0
            model.train()
            for _,data in enumerate(training_loader, 0):
                ids = data['ids'].to(device, dtype = torch.long)
                mask = data['mask'].to(device, dtype = torch.long)
                targets = data['targets'].to(device, dtype = torch.long)

                outputs = model(ids, mask)
                loss = loss_function(outputs, targets)
                tr_loss += loss.item()
                big_val, big_idx = torch.max(outputs.data, dim=1)
                n_correct += calcuate_accu(big_idx, targets)

                nb_tr_steps += 1
                nb_tr_examples+=targets.size(0)
                
                if _%10==0:
                    loss_step = tr_loss/nb_tr_steps
                    accu_step = (n_correct*100)/nb_tr_examples 
                    # print(f"Training Loss per 100 steps: {loss_step}")
                    # print(f"Training Accuracy per 100 steps: {accu_step}")

                optimizer.zero_grad()
                loss.backward()
                # # When using GPU
                optimizer.step()

            
            epoch_loss = tr_loss/nb_tr_steps
            epoch_accu = (n_correct*100)/nb_tr_examples
            if (epoch+1) % 10 == 0:
                print(f'The Total Accuracy for Epoch {epoch}: {(n_correct*100)/nb_tr_examples}')
                
                print(f"Training Loss Epoch: {epoch_loss}")
                print(f"Training Accuracy Epoch: {epoch_accu}")

            return epoch_loss, epoch_accu


        for epoch in range(EPOCHS):
            epoch_loss, epoch_accu = train(epoch)

        model_acc_loss.append({"storyId":storyid,
            "unitId":obj,
            "accuracy": epoch_accu,
            "loss": epoch_loss})

        output_model_file = 'models/'+str(storyid)+"/"+str(obj)

        if os.path.isdir(output_model_file):
            shutil.rmtree(output_model_file)
            os.mkdir(output_model_file)
        else:
            os.mkdir(output_model_file)

        torch.save(model, output_model_file+'/model')
        response = requests.post("http://52.170.194.186:6001/load", data = {"storyid": storyid, "unitid": obj})  
        # torch.save(model.classifier, output_model_file+'/classifier')

    with open(output_models_story+'/data.json', 'w') as outfile:
        json.dump(data, outfile, indent=4)
    with open(output_models_story+'/config.json', 'w') as outfile:
        json.dump(config, outfile, indent=4)
    model_acc_loss = {"uuid": data['uuid'], "model_acc_loss": model_acc_loss}
    with open(output_models_story+'/model_acc_loss.json', 'w') as outfile:
        json.dump(model_acc_loss, outfile, indent=4)


    print("end train and return")
    return web.json_response(model_acc_loss)

app = web.Application()

app.add_routes(routes)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
})

for route in list(app.router.routes()):
    cors.add(route)
#web.run_app(app)
web.run_app(app,host='0.0.0.0', port = '6000')
